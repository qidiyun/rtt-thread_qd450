
#include <rtdevice.h>
#include <rthw.h>
#include "gd32f4xx.h"
#include "gd32f4xx_exti.h"


rt_device_t my_device;

/* common device interface */
rt_err_t  mydevice_init(rt_device_t dev)
{
	rt_kprintf("mydevice_init\r\n");
}
rt_err_t mydevice_open(rt_device_t dev, rt_uint16_t oflag)
{
	rt_kprintf("mydevice_open\r\n");
}
rt_err_t mydevice_close(rt_device_t dev)
{
	rt_kprintf("mydevice_close\r\n");
}
rt_size_t mydevice_read(rt_device_t dev, rt_off_t pos, void *buffer, rt_size_t size)
{
	rt_kprintf("mydevice_read\r\n");
}
rt_size_t mydevice_write(rt_device_t dev, rt_off_t pos, const void *buffer, rt_size_t size)
{
	rt_kprintf("mydevice_write\r\n");
}
rt_err_t  mydevice_control(rt_device_t dev, int cmd, void *args)
{
	rt_kprintf("mydevice_control\r\n");
}


#ifdef RT_USING_DEVICE_OPS

const struct rt_device_ops mydevice_ops = {
	
    /* common device interface */
    .init = mydevice_init,
    .open = mydevice_open,
    .close = mydevice_close,
    .read = mydevice_read,
    .write = mydevice_write,
    .control = mydevice_control,
	
};

#endif


void mydevice_create(void)
{
	my_device = rt_device_create(RT_Device_Class_Char, 0);
	if(my_device == NULL)
	{
		rt_kprintf("mydevice_create error \r\n");
		return ;
	}

#ifdef RT_USING_DEVICE_OPS
    my_device->ops = &mydevice_ops;
#else
    /* common device interface */
    my_device->init = mydevice_init,
    my_device->open = mydevice_open,
    my_device->close = mydevice_close,
    my_device->read = mydevice_read,
    my_device->write = mydevice_write,
    my_device->control = mydevice_control,
#endif

	rt_device_register(my_device, "mydevice", RT_DEVICE_FLAG_RDWR);
}

INIT_DEVICE_EXPORT(mydevice_create);

void mydevice_test(void)
{
	char mydev_buf[10];
	rt_device_t mydev;

	mydev = rt_device_find("mydevice");

	rt_device_init(mydev);
	rt_device_open(mydev, RT_DEVICE_OFLAG_RDWR);

	rt_device_control(mydev, RT_DEVICE_CTRL_CONFIG, NULL);

	rt_device_read(mydev, 0, mydev_buf, 10);

	rt_device_write(mydev, 0, mydev_buf, 10);

	rt_device_close(mydev);
}

