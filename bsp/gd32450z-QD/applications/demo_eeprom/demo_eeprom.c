/*
 * File      : main.c
 * This file is part of RT-Thread RTOS
 * COPYRIGHT (C) 2006 - 2018, RT-Thread Development Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-05-18     tanek        first implementation
 */

#include <stdio.h>
#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>
#include "at24cxx.h"



at24cxx_device_t at24_dev;

unsigned char *wr_ep_data = "~!@#$";
unsigned char rx_ep_data[10];


int i2c_eeprom_sample(void)
{
    at24_dev = at24cxx_init("i2c0", 0x00);

	at24cxx_write(at24_dev, 4, wr_ep_data, 5);
	

	at24cxx_read(at24_dev, 0, rx_ep_data, 9);
	rt_kprintf("eeprom read [%s]\r\n", rx_ep_data);
	
}

