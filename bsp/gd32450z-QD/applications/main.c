/*
 * File      : main.c
 * This file is part of RT-Thread RTOS
 * COPYRIGHT (C) 2006 - 2018, RT-Thread Development Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-05-18     tanek        first implementation
 */

#include <stdio.h>
#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>
#include "demo_hello/hello.h"

int i2c_eeprom_sample(void);
void pin_key_sample(void);

/* defined the LED0 pin: PF9 */
#define LED0_PIN    GET_PIN(D, 4)


int rt_gui_demo_init(void);


int gd32_hw_lcd_init(void);
	

rt_device_t lcd;

int main(void)
{
    int count = 1;

	demo_hello();
	
    /* set LED0 pin mode to output */
    rt_pin_mode(LED0_PIN, PIN_MODE_OUTPUT);
	
		tcpclient_init();
	
//		rt_thread_mdelay(100);  //????

	//lcd_test();
		
    lcd = rt_device_find("lcd");
		rtgui_graphic_set_device(lcd);
	
		rt_gui_demo_init();
//	
//	i2c_eeprom_sample();
//		
//	pin_key_sample();
//	
    while (count++)
    {
        rt_pin_write(LED0_PIN, PIN_HIGH);
        rt_thread_mdelay(500);
        rt_pin_write(LED0_PIN, PIN_LOW);
        rt_thread_mdelay(500);
    }

    return RT_EOK;
}

#define KEY0_PIN_NUM    GET_PIN(C, 13)

void key_on(void *args)
{
    rt_kprintf("key_on !\n");
}


void pin_key_sample(void)
{

    /* ??0??????? */
    rt_pin_mode(KEY0_PIN_NUM, PIN_MODE_INPUT_PULLUP);
    /* ????,?????,??????beep_on */
    rt_pin_attach_irq(KEY0_PIN_NUM, PIN_IRQ_MODE_FALLING, key_on, RT_NULL);
    /* ???? */
    rt_pin_irq_enable(KEY0_PIN_NUM, PIN_IRQ_ENABLE);

}


